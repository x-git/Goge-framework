## 第一步

导入一个jar包

## 第二步

创建一个配置文件

## 第三步

没了......

## 说明

需要引入自己项目的jar包为：

````
<dependency>
    <groupId>com.gitee.sherlockholmnes</groupId>
    <artifactId>goge-start-all</artifactId>
    <version>0.1.2</version>
</dependency>
````

## 开发文档

点击wiki导航菜单即可查看

配置文件在 goge-start-all 的resource目录下 有一个demo

有什么问题的话，可以加我的QQ群：773291321