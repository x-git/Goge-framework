package com.yuyenews.core.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 文件帮助
 * @author yuye
 *
 */
public class FileUtil {
	
	private static Logger logger = LoggerFactory.getLogger(FileUtil.class);
	
	public static String local = null;

	/**
	 * 根据文件路径 获取文件中的字符串内容
	 * @param path 路径
	 * @return str
	 */
	public static String readFileString(String path) {
		
		try {
			InputStream inputStream = FileUtil.class.getResourceAsStream(path);
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));  
			StringBuffer sb = new StringBuffer();  
			String str = "";
			while ((str = reader.readLine()) != null)  
			{  
			    sb.append(str);  
			}  
			return sb.toString();
		} catch (Exception e) {
			if(local == null) {
				logger.error("",e);
			} else {
				logger.warn("自定义mybatis配置文件加载失败或者不存在，将自动使用默认配置");
			}
		}
		return null;
	}

	/**
	 * 根据文件路径 获取yml配置文件
	 * @param path 路径
	 * @return str
	 */
	public static String readYml(String path) {

		try {
			InputStream inputStream = FileUtil.class.getResourceAsStream(path);
			HashMap testEntity = org.ho.yaml.Yaml.loadType(inputStream, HashMap.class);//如果是读入Map,这里不可以写Map接口，必须写实现
			return JSON.toJSONString(testEntity);
		} catch (Exception e) {
			logger.error("",e);
		}
		return null;
	}
}
