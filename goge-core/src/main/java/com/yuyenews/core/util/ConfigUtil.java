package com.yuyenews.core.util;

import com.alibaba.fastjson.JSONObject;
import com.yuyenews.core.constant.EasySpace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 配置文件工具类
 * @author yuye
 *
 */
public class ConfigUtil {

	private static Logger log = LoggerFactory.getLogger(ConfigUtil.class);
	
	private static EasySpace constants = EasySpace.getEasySpace();


	/**
	 * 加载配置文件
	 */
	public static void loadConfig() {
		try {
			String content = FileUtil.readYml("/goge.yml");

			JSONObject object = JSONObject.parseObject(content);

			constants.setAttr("config", object);
		} catch (Exception e) {
			log.error("加载配置文件报错",e);
		}
	}


	/**
	 * 获取配置信息
	 * @return json
	 */
	public static JSONObject getConfig() {
		Object obj = constants.getAttr("config");
		if(obj != null) {
			JSONObject jsonObject = (JSONObject)obj;
			
			return jsonObject;
		}
		
		return null;
	}
}
