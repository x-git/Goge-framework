package com.yuyenews.start;

import com.yuyenews.easy.init.InitJdbc;

/**
 * 初始化扩展模块
 * 
 * @author yuye
 *
 */
public final class InitExtends {

	/**
	 * 加载扩展模块
	 */
	protected static void loadExtends() {
		loadJdbc();
	}

	/**
	 * 加载JDBC模块
	 */
	private static void loadJdbc() {

		InitJdbc initJdbc = new InitJdbc();
		initJdbc.init();

	}
}
